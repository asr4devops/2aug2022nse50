import os, sys
import sqlite3
from sqlite3 import Error
import csv


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

def create_project(conn, project):
    """
    Create a new project into the projects table
    :param conn:
    :param project:
    :return: project id
    """
    sql = ''' INSERT INTO sharename(name)
              VALUES(?) '''
    cur = conn.cursor()
    cur.execute(sql, project)
    conn.commit()
    return cur.lastrowid


def create_task(conn, columns, placeholders, task):
    """
    Create a new task
    :param conn:
    :param task:
    :return:
    """

    # sql = ''' INSERT INTO tasks(name,priority,status_id,project_id,begin_date,end_date)
            #   VALUES(?,?,?,?,?,?) '''

    sql = 'INSERT INTO tasks({}) VALUES ({})'.format(columns,placeholders)
    cur = conn.cursor()
    cur.execute(sql, task)
    conn.commit()
    return cur.lastrowid

def main():
    database = r"pythonsqlite.db"

    sql_create_sharedetails_table = """CREATE TABLE IF NOT EXISTS sdetails (
                                    id integer PRIMARY KEY,
                                    Symbol text NOT NULL, 
                                    Series text NOT NULL,
                                    Date text NOT NULL, 
                                    Prev Close integer NOT NULL,
                                    Open Price integer NOT NULL, 
                                    High Price integer NOT NULL, 
                                    Low Price integer NOT NULL, 
                                    Last Price integer NOT NULL, 
                                    Close Price integer NOT NULL, 
                                    Average Price integer NOT NULL, 
                                    Total Traded Quantity integer, 
                                    Turnover integer, 
                                    No. of Trades integer, 
                                    Deliverable Qty integer, 
                                    % Dly Qt to Traded Qty integer
                                );"""

    # create a database connection
    conn = create_connection(database)

    # create tables
    if conn is not None:

        # create tasks table
        # create_table(conn, sql_create_sharedetails_table)
    

        curPath = os.getcwd()
        listoffiles = os.listdir(curPath)
        for i in listoffiles:
            if i.endswith('.csv'):
                with open(i, mode='r') as csv_file:
                    csv_reader = csv.DictReader(csv_file)
                    line_count = 0
                    for row in csv_reader:
                        if line_count == 0:
                            print(f'Column names are {", ".join(row)}')
                            line_count += 1
                        columns = "', '".join(row.keys())
                        placeholders = ', '.join('?' * len(row))
                        # sql = 'INSERT INTO Media ({}) VALUES ({})'.format(columns, placeholders)
                        values = [str(x).strip() if isinstance(x, bool) else str(x).strip() for x in row.values()]
                        values = tuple(values)
                        columns = "'"+str(columns)+"'"
                        with conn:
                            sql = 'INSERT INTO sDetails ({}) VALUES {}'.format(columns,values)
                            cur = conn.cursor()
                            print(sql)
                            cur.execute(sql)
                            conn.commit()
                            print("[+] inserted ",cur.lastrowid)
                            # create_task(conn,columns,placeholders,values)

    else:
        print("Error! cannot create the database connection.")


if __name__ == '__main__':
    main()
    
            

    